package Modelss;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by olek9 on 18.10.2017.
 */
public class Season {

   private int seasonNumber;
   private int yearOfRelease;

   List<Episode> episodes = new ArrayList<Episode>();

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }
}
