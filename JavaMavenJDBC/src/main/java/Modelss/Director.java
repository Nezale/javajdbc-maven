package Modelss;

import java.time.LocalDate;

/**
 * Created by olek9 on 18.10.2017.
 */
public class Director {

    String name;
    LocalDate dateOBirth;
    String biography;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateOBirth() {
        return dateOBirth;
    }

    public void setDateOBirth(LocalDate dateOBirth) {
        this.dateOBirth = dateOBirth;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }
}
