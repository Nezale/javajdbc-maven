package Modelss;

import java.time.Duration;
import java.time.LocalDate;

/**
 * Created by olek9 on 18.10.2017.
 */
public class Episode {

    String name;
    LocalDate releaseDate;
    int episodeNumber;
    Duration duration;

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getName() {


        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
